use lambda_http::{run, service_fn, tracing, Body, Error, Request, RequestExt, Response};
use std::convert::Infallible;
use std::io::Write;
use std::path::PathBuf;

// Function to process the prompt and generate a response using the transformer model
fn generate_response(input_prompt: String) -> Result<String, Box<dyn std::error::Error>> {
    // Initialize tokenizer and model architecture settings
    let tokenizer_setting = llm::TokenizerSource::Embedded;
    let model_arch = llm::ModelArchitecture::GptNeoX;

    // Path to the local model for local testing
    //let local_model_path = PathBuf::from("src/pythia-1b-q4_0-ggjt.bin");
    // Uncomment the following line for the deployment model path
    let deploy_model_path = PathBuf::from("/new-lambda-project/pythia-1b-q4_0-ggjt.bin");

    // Load the model with specified configurations
    let transformer_model = llm::load_dynamic(
        Some(model_arch),
        &deploy_model_path,
        tokenizer_setting,
        Default::default(),
        llm::load_progress_callback_stdout,
    )?;

    // Start a new model session
    let mut model_session = transformer_model.start_session(Default::default());
    let mut collected_response = String::new();

    // Perform inference with the provided prompt
    let inference_outcome = model_session.infer::<Infallible>(
        transformer_model.as_ref(),
        &mut rand::thread_rng(),
        &llm::InferenceRequest {
            prompt: (&input_prompt).into(),
            parameters: &llm::InferenceParameters::default(),
            play_back_previous_tokens: false,
            maximum_token_count: Some(18),
        },
        &mut Default::default(),
        |response| match response {
            llm::InferenceResponse::PromptToken(token) | llm::InferenceResponse::InferredToken(token) => {
                print!("{token}");
                std::io::stdout().flush().unwrap();
                collected_response.push_str(&token);
                Ok(llm::InferenceFeedback::Continue)
            }
            _ => Ok(llm::InferenceFeedback::Continue),
        },
    );

    // Return the final response or error
    match inference_outcome {
        Ok(_) => Ok(collected_response),
        Err(e) => Err(Box::new(e)),
    }
}

// Lambda function handler to process requests and send responses
async fn handle_request(req: Request) -> Result<Response<Body>, Error> {
    let user_query = req
        .query_string_parameters_ref()
        .and_then(|params| params.first("query"))
        .unwrap_or("This project is hard");

    let response_message = match generate_response(user_query.to_string()) {
        Ok(result) => result,
        Err(e) => format!("Inference error: {:?}", e),
    };
    println!("Response from model: {:?}", response_message);

    let response = Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(Body::from(response_message))
        .map_err(Box::new)?;

    Ok(response)
}

// Main function to initiate the Lambda service
#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();
    run(service_fn(handle_request)).await
}
