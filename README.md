# 🤖 Holly's Rust Serverless Transformer Endpoint

<blockquote>
<i> <b>This project is hard</b> to describe. It's like a mix of magic and technology ...</i><br/>
—— Completed by my model (with the bold texts as prompt)
</blockquote>

## Description

This repository houses a serverless [Rust](https://www.rust-lang.org/) application designed to integrate a [Hugging Face transformer LLM](https://huggingface.co/rustformers) for prompted language generation, and deployed to [AWS ECR](https://aws.amazon.com/ecr/) and [AWS Lambda](https://aws.amazon.com/lambda/) using [Docker](https://www.docker.com/) image. The application features an efficient query endpoint for `curl` invocation through Lambda URL, offering a scalable solution for machine learning and cloud computing tasks. 

- **Model**: In order to enable LLM with Rust, our choices of model are limited to [a narrow scope](https://huggingface.co/rustformers). Due to the limitations of AWS memory size and response time, only several models are practically executable. After rounds of trial-and-error, GGML converted versions of *EleutherAI's* [*Pythia*](https://huggingface.co/rustformers/pythia-ggml) models are proven to be stable and yield acceptable response time with size in lightweight. Among the 8 size options in *Pythia*, `pythia-1b-q4_0-ggjt` offers the best fit for our objectives. (Anything above 1B takes too much space and time for AWS to host; below 1B generates texts that are slightly off; and some failed with Rust integration)

- **Library**: To enable the LLM inference endpoint, the `rustformers` library is added as a dependency directly from [source](https://github.com/rustformers/llm). Example use cases and templates for integration can also be found here.

- **Deployment**: The local project is deployed to a repository on AWS ECR with a dockerized image. It is then connected with AWS Lambda for remote invoke: https://x245bk3rdjzmsfvfgk5i47vude0nxjov.lambda-url.us-east-1.on.aws/ 

- **Invoke**: To send a remote invoke through the URL above, we can use
```shell
curl -X POST https://x245bk3rdjzmsfvfgk5i47vude0nxjov.lambda-url.us-east-1.on.aws/ -H "Content-Type: application/json" -d '{"text":"This project is hard"}'
```

## Demo

- **Local Test**

![local](https://gitlab.com/hollyyfc/hollycui_rustllm/-/wikis/uploads/53ccf11f771d96b2b820975a99700146/local.png)

- **Deployment**

![ecr](https://gitlab.com/hollyyfc/hollycui_rustllm/-/wikis/uploads/faab76b7ea328e4bd318c45bfa8b7a85/ecr.png)

![lambda](https://gitlab.com/hollyyfc/hollycui_rustllm/-/wikis/uploads/e9ee931b6edc78a0d77931c935b9dddf/lambda.png)

- **Remote Invocation & Logs**

![curl](https://gitlab.com/hollyyfc/hollycui_rustllm/-/wikis/uploads/36169e1193f30a7ff050af94c03ea68d/curl.png)

![log](https://gitlab.com/hollyyfc/hollycui_rustllm/-/wikis/uploads/fa4e81f9580b360cf87525499368aee3/log.png)

![log_copy](https://gitlab.com/hollyyfc/hollycui_rustllm/-/wikis/uploads/596c7dcfc3156d5f4198245a3580ac7c/log_copy.png)


## Steps Walkthrough

- `cargo lambda new <PROJECT-NAME>` in desired directory with HTTP enabled
- `cargo add <crate_name> --features "<feature_name>"`
    - Manually add `llm = { git = "https://github.com/rustformers/llm" , branch = "main" }` according to the [official documentation](https://github.com/rustformers/llm)
- Choose a model and adopt the template according to `rustformers` website
    - For local test, make sure to download the model and place it in project directory
    - Beware of diverging model paths for local test and deployment!
    - Finetune the inference endpoint code (e.g., default prompt texts, parameters, maximum output tokens, etc.)
    - Define / locate listening port (mine is on `localhost:9000`)
- `cargo lambda watch` for local test

---

- <ins>AWS IAM</ins>: Create a new IAM user with the follwing policies attached
    - `IAMFullAccess`
    - `AWSLambda_FullAccess`
    - `AWSAppRunnerServicePolicyForECRAccess`
    - `EC2InstanceProfileForImageBuilderECRContainerBuilds`
- Reconfigure local terminal: `aws configure` with the new IAM's `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION`
- <ins>AWS ECR</ins>: Create a new repository (There should be a personalized guide on dockerizing project and pushing to ECR in your repo)
- Dockerize project:
    - Open your local Docker
    - Retrieve an authentication token and authenticate your Docker client:
    ```shell
    aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin <YOUR_ACCOUNT>.dkr.ecr.us-east-1.amazonaws.com
    ```
    - Write a `Dockerfile` in your local project directory, specifying containerization steps
    - (Make sure you fix your model path in `main.rs` to match with the deployed directory)
    - Build a Docker image: Make sure `YOUR_PROJECT_NAME` matches that in your `Dockerfile`
    ```shell
    docker buildx build --progress=plain --platform linux/arm64 -t <YOUR_PROJECT_NAME> .
    ```
    - Tag your image: 
    ```shell
    docker tag <PERSONAL_IMAGE_ID>
    ```
    - Push this image to the newly created repo: (*IT TAKES LOOOONG so dw*)
    ```shell
    docker push <PERSONAL_IMAGE_ID>
    ```

---

- <ins>AWS Lambda</ins>: Create a new function
    - Use the *Container Image* option
    - Choose AWS ECR -> your new repo
    - Select `arm64` (for macOS)
- (Resource control: `Configuration` -> `General Configuration` -> Change max memory limit and max timeout -> plz don't get surprising bill 🙏)
- Generate invoke URL: `Configuration` -> `Fucntion URL` -> Create a new function URL -> Choose `NONE` & `BUFFERED` & enable `CORS` -> Save the URL
- Test remote invoke from your local terminal (the `curl` command above)
    - *IT TAKES LOOOONG AGAIN* depending on your max number of output tokens and model size
    - From my CloudWatch logs, the average reponse time is 6.5 min

---

**Note**: As a final step, to push all local files to GitLab, the model file has to be transformed a bit as of its massive size. Here are my steps with the use of [Git LFS](https://git-lfs.com/):

0. Install Git LFS (for macOS)
```shell
brew install git-lfs
```
1. Initialize Git LFS in Your Repository
```shell
git lfs install
```
2. Track Large Files
```shell
git lfs track "*.bin"
```
This will create a `.gitattributes` file. Don't forget to `git add` this file or LFS won't be working. 

3. Your usual add, commit, and push
4. To pull with LFS
```shell
git lfs pull
```